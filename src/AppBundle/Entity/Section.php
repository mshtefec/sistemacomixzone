<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Section
 *
 * @ORM\Table(name="section")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SectionRepository")
 */
class Section
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="sequence", type="integer")
     */
    private $sequence;

    /**
     * @var bool
     *
     * @ORM\Column(name="isSubsection", type="boolean", nullable=true)
     */
    private $isSubsection;

    /**
     * @var string
     *
     * @ORM\Column(name="located", type="string", length=255)
     */
    private $located;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Section
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     *
     * @return Section
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return int
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set isSubsection
     *
     * @param boolean $isSubsection
     *
     * @return Section
     */
    public function setIsSubsection($isSubsection)
    {
        $this->isSubsection = $isSubsection;

        return $this;
    }

    /**
     * Get isSubsection
     *
     * @return bool
     */
    public function getIsSubsection()
    {
        return $this->isSubsection;
    }

    /**
     * Set located
     *
     * @param string $located
     *
     * @return Section
     */
    public function setLocated($located)
    {
        $this->located = $located;

        return $this;
    }

    /**
     * Get located
     *
     * @return string
     */
    public function getLocated()
    {
        return $this->located;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Section
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }
}

