<?php

namespace ComicBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ComicBundle\Entity\Comic;
use ComicBundle\Form\ComicType;

/**
 * Comic controller.
 *
 * @Route("/admin/comics")
 */
class ComicController extends Controller
{
    /**
     * Lists all Comic entities.
     *
     * @Route("/", name="comics_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $comics = $em->getRepository('ComicBundle:Comic')->findAll();

        return $this->render('comic/index.html.twig', array(
            'comics' => $comics,
        ));
    }

    /**
     * Creates a new Comic entity.
     *
     * @Route("/new", name="comics_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $comic = new Comic();
        $form = $this->createForm('ComicBundle\Form\ComicType', $comic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comic);
            $em->flush();

            return $this->redirectToRoute('comics_show', array('id' => $comic->getId()));
        }

        return $this->render('comic/new.html.twig', array(
            'comic' => $comic,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Comic entity.
     *
     * @Route("/{id}", name="comics_show")
     * @Method("GET")
     */
    public function showAction(Comic $comic)
    {
        $deleteForm = $this->createDeleteForm($comic);

        return $this->render('comic/show.html.twig', array(
            'comic' => $comic,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Comic entity.
     *
     * @Route("/{id}/edit", name="comics_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Comic $comic)
    {
        $deleteForm = $this->createDeleteForm($comic);
        $editForm = $this->createForm('ComicBundle\Form\ComicType', $comic);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comic);
            $em->flush();

            return $this->redirectToRoute('comics_edit', array('id' => $comic->getId()));
        }

        return $this->render('comic/edit.html.twig', array(
            'comic' => $comic,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Comic entity.
     *
     * @Route("/{id}", name="comics_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Comic $comic)
    {
        $form = $this->createDeleteForm($comic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comic);
            $em->flush();
        }

        return $this->redirectToRoute('comics_index');
    }

    /**
     * Creates a form to delete a Comic entity.
     *
     * @param Comic $comic The Comic entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Comic $comic)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('comics_delete', array('id' => $comic->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
