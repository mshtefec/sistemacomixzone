<?php

namespace ComicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Detail
 *
 * @ORM\Table(name="detail")
 * @ORM\Entity(repositoryClass="ComicBundle\Repository\DetailRepository")
 */
class Detail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="artBy", type="text", nullable=true)
     */
    private $artBy;

    /**
     * @var string
     *
     * @ORM\Column(name="writtenBy", type="text", nullable=true)
     */
    private $writtenBy;

    /**
     * @var string
     *
     * @ORM\Column(name="backupArtBy", type="text", nullable=true)
     */
    private $backupArtBy;

    /**
     * @var string
     *
     * @ORM\Column(name="backupWrittenBy", type="text", nullable=true)
     */
    private $backupWrittenBy;

    /**
     * @var string
     *
     * @ORM\Column(name="coverBy", type="text", nullable=true)
     */
    private $coverBy;

    /**
     * @var string
     *
     * @ORM\Column(name="variantCoverBy", type="text", nullable=true)
     */
    private $variantCoverBy;

    /**
     * @var int
     *
     * @ORM\Column(name="series", type="integer", nullable=true)
     */
    private $series;

    /**
     * @var string
     *
     * @ORM\Column(name="priceIn", type="text", nullable=true)
     */
    private $priceIn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="onSale", type="datetime", nullable=true)
     */
    private $onSale;

    /**
     * @var int
     *
     * @ORM\Column(name="issue", type="integer", nullable=true)
     */
    private $issue;

    /**
     * @var string
     *
     * @ORM\Column(name="colorOrBw", type="string", length=255, nullable=true)
     */
    private $colorOrBw;

    /**
     * @var int
     *
     * @ORM\Column(name="countPages", type="integer", nullable=true)
     */
    private $countPages;

    /**
     * @var string
     *
     * @ORM\Column(name="images", type="string", length=255)
     */
    private $images;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set artBy
     *
     * @param string $artBy
     *
     * @return Detail
     */
    public function setArtBy($artBy)
    {
        $this->artBy = $artBy;

        return $this;
    }

    /**
     * Get artBy
     *
     * @return string
     */
    public function getArtBy()
    {
        return $this->artBy;
    }

    /**
     * Set writtenBy
     *
     * @param string $writtenBy
     *
     * @return Detail
     */
    public function setWrittenBy($writtenBy)
    {
        $this->writtenBy = $writtenBy;

        return $this;
    }

    /**
     * Get writtenBy
     *
     * @return string
     */
    public function getWrittenBy()
    {
        return $this->writtenBy;
    }

    /**
     * Set backupArtBy
     *
     * @param string $backupArtBy
     *
     * @return Detail
     */
    public function setBackupArtBy($backupArtBy)
    {
        $this->backupArtBy = $backupArtBy;

        return $this;
    }

    /**
     * Get backupArtBy
     *
     * @return string
     */
    public function getBackupArtBy()
    {
        return $this->backupArtBy;
    }

    /**
     * Set backupWrittenBy
     *
     * @param string $backupWrittenBy
     *
     * @return Detail
     */
    public function setBackupWrittenBy($backupWrittenBy)
    {
        $this->backupWrittenBy = $backupWrittenBy;

        return $this;
    }

    /**
     * Get backupWrittenBy
     *
     * @return string
     */
    public function getBackupWrittenBy()
    {
        return $this->backupWrittenBy;
    }

    /**
     * Set coverBy
     *
     * @param string $coverBy
     *
     * @return Detail
     */
    public function setCoverBy($coverBy)
    {
        $this->coverBy = $coverBy;

        return $this;
    }

    /**
     * Get coverBy
     *
     * @return string
     */
    public function getCoverBy()
    {
        return $this->coverBy;
    }

    /**
     * Set variantCoverBy
     *
     * @param string $variantCoverBy
     *
     * @return Detail
     */
    public function setVariantCoverBy($variantCoverBy)
    {
        $this->variantCoverBy = $variantCoverBy;

        return $this;
    }

    /**
     * Get variantCoverBy
     *
     * @return string
     */
    public function getVariantCoverBy()
    {
        return $this->variantCoverBy;
    }

    /**
     * Set series
     *
     * @param integer $series
     *
     * @return Detail
     */
    public function setSeries($series)
    {
        $this->series = $series;

        return $this;
    }

    /**
     * Get series
     *
     * @return int
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * Set priceIn
     *
     * @param string $priceIn
     *
     * @return Detail
     */
    public function setPriceIn($priceIn)
    {
        $this->priceIn = $priceIn;

        return $this;
    }

    /**
     * Get priceIn
     *
     * @return string
     */
    public function getPriceIn()
    {
        return $this->priceIn;
    }

    /**
     * Set onSale
     *
     * @param \DateTime $onSale
     *
     * @return Detail
     */
    public function setOnSale($onSale)
    {
        $this->onSale = $onSale;

        return $this;
    }

    /**
     * Get onSale
     *
     * @return \DateTime
     */
    public function getOnSale()
    {
        return $this->onSale;
    }

    /**
     * Set issue
     *
     * @param integer $issue
     *
     * @return Detail
     */
    public function setIssue($issue)
    {
        $this->issue = $issue;

        return $this;
    }

    /**
     * Get issue
     *
     * @return int
     */
    public function getIssue()
    {
        return $this->issue;
    }

    /**
     * Set colorOrBw
     *
     * @param string $colorOrBw
     *
     * @return Detail
     */
    public function setColorOrBw($colorOrBw)
    {
        $this->colorOrBw = $colorOrBw;

        return $this;
    }

    /**
     * Get colorOrBw
     *
     * @return string
     */
    public function getColorOrBw()
    {
        return $this->colorOrBw;
    }

    /**
     * Set countPages
     *
     * @param integer $countPages
     *
     * @return Detail
     */
    public function setCountPages($countPages)
    {
        $this->countPages = $countPages;

        return $this;
    }

    /**
     * Get countPages
     *
     * @return int
     */
    public function getCountPages()
    {
        return $this->countPages;
    }

    /**
     * Set images
     *
     * @param string $images
     *
     * @return Detail
     */
    public function setImages($images)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Get images
     *
     * @return string
     */
    public function getImages()
    {
        return $this->images;
    }
}

