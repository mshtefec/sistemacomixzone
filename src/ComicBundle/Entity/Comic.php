<?php

namespace ComicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comic
 *
 * @ORM\Table(name="comic")
 * @ORM\Entity(repositoryClass="ComicBundle\Repository\ComicRepository")
 */
class Comic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="authorscript", type="string", length=100)
     */
    private $authorscript;

    /**
     * @var string
     *
     * @ORM\Column(name="authordraw", type="string", length=100)
     */
    private $authordraw;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="cover", type="string", length=255)
     */
    private $cover;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Comic
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set authorscript
     *
     * @param string $authorscript
     *
     * @return Comic
     */
    public function setAuthorscript($authorscript)
    {
        $this->authorscript = $authorscript;

        return $this;
    }

    /**
     * Get authorscript
     *
     * @return string
     */
    public function getAuthorscript()
    {
        return $this->authorscript;
    }

    /**
     * Set authordraw
     *
     * @param string $authordraw
     *
     * @return Comic
     */
    public function setAuthordraw($authordraw)
    {
        $this->authordraw = $authordraw;

        return $this;
    }

    /**
     * Get authordraw
     *
     * @return string
     */
    public function getAuthordraw()
    {
        return $this->authordraw;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Comic
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set cover
     *
     * @param string $cover
     *
     * @return Comic
     */
    public function setCover($cover)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }
}

