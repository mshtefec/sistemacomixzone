<?php

namespace ComicBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
/**
 * UserType form.
 * @author Max Shtefec <max.shtefec@gmail.com>
 */
class UserType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('username', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('email', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'ComicBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'comicbundle_user';
    }

}
